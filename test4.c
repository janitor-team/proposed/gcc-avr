volatile int cnt;
int main() {
  while (1) cnt++;
  return 0;
}

/*
avr-gcc -DMASTER -Wall -Wstrict-prototypes -g2 -Os -mmcu=atmega128 -mno-tablejump -fpack-struct -fno-common -gdwarf-2 -mcall-prologues -DSTACK_OVERFLOW_CHECK -D'FIRMWARE_BACKUP_START=(0x10000L)' -D'FIRMWARE_BACKUP_END=((0x10000L + (0xE000L - 0L)))' -D'FIRMWARE_START=(0L)' -D'FIRMWARE_END=(0xE000L)' -D'BOOTLOADER_START=(0x1E000L)' -D'BOOTLOADER_END=(0x1FC00L)' -I/srv/home/michael/Projects/rtm/rtm5101 -I/srv/home/michael/Projects/rtm/rtm5101/include -c -o test4.o test4.c
avr-ld -mavr5 test4.o /usr/lib/gcc/avr/4.3.4/avr51/libgcc.a -o firmware.elf
 */

