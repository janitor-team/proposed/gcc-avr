#!/bin/bash

for f in `find gcc -type f -name \*.1 -or -name \*.texi.in -or -name \*.texi -or -name \*.html -or -name \*.7 -or -name \*.info -or -name \*.rst`; do
    echo > $f
done
rm -r gcc/libjava
rm `find gcc -type f -name \*.chm` || true


