#!/bin/bash

if [ $# -ne 2 ]; then 
    echo Usage: $0 '<archive>' '<version>'
    echo Exmpl: $0 avr-gcc.tar.bz2 5.4.0+Atmel3.6.0
    exit -1
fi

if [ ! -z "`git diff`" ]; then
    echo Please commit first
    exit
fi

VER=$2

set -x
set -e

git checkout upstream
rm -Rf gcc
tar xf $1
git add gcc
git commit -m "Import upstream version $VER"
git tag upstream/$VER
git push
git push --tags

git checkout dfsg_clean
git merge --no-commit -X theirs upstream
./make_dfsg.sh
git add gcc
git commit -m "Make upstream version $VER dfsg-clean"
git tag "dfsg_clean/$VER"
git push
git push --tags

git checkout master
git merge dfsg_clean -m "Merge dfsg-clean version of upstream $VER"

dch -v 1:$VER-1 New upstream release
dch -r ok
git add debian/changelog

vim debian/control
git add debian/control

git commit -m 'Release message in changelog and new versioned deps'

git tag "debian/$VER-1"
git push
git push --tags

gbp buildpackage --git-pbuilder --git-upstream-tree=branch --git-upstream-branch=dfsg_clean

